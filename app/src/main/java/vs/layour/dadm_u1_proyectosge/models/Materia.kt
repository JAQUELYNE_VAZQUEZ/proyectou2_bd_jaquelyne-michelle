package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Materia(
    @SerializedName("calificacion")
    val calificacion: String,
    @SerializedName("clave")
    val clave: String,
    @SerializedName("horario")
    val horario: String,
    @SerializedName("materia")
    val nombre: String,
    @SerializedName("profesor")
    val profesor: String
): Serializable