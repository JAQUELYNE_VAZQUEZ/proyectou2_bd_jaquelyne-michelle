package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Vierne(
    @SerializedName("hora")
    val hora: String,
    @SerializedName("materia")
    val materia: String,
    @SerializedName("profesor")
    val profesor: String
): Serializable