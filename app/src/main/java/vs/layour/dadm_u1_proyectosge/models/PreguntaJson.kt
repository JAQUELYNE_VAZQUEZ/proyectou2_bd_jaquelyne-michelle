package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName

data class PreguntaJson(
    @SerializedName("preguntas")
    val preguntas: ArrayList<Pregunta>
)