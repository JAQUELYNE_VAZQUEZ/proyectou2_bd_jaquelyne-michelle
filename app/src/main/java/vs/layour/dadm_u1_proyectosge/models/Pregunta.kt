package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName

data class Pregunta(
    @SerializedName("pregunta")
    val pregunta: String,
    @SerializedName("rubro")
    val rubro: String
)