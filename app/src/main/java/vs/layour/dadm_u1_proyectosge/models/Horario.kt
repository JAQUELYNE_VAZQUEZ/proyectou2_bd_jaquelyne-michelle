package vs.layour.dadm_u1_proyectosge.models


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Horario(
    @SerializedName("jueves")
    val jueves: ArrayList<Jueve>,
    @SerializedName("lunes")
    val lunes: ArrayList<Lune>,
    @SerializedName("martes")
    val martes: ArrayList<Marte>,
    @SerializedName("miercoles")
    val miercoles: ArrayList<Miercole>,
    @SerializedName("viernes")
    val viernes: ArrayList<Vierne>
):Serializable