package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.Profesore

class MenuProfesorActivity : AppCompatActivity() {

    lateinit var btnMateriasProfesor: Button
    lateinit var btnAlumnosTotalesProfesor: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_profesor)

        btnMateriasProfesor = findViewById(R.id.btnMenuProfesor_Materias)
        btnAlumnosTotalesProfesor = findViewById(R.id.btnMenuProfesor_AlumnosTotales)


        val database = Utils.dataBaseSQL
       // database.altaEvaluacion(Evaluacion(0,"Lua Vargas","18120225","15:30","Comentario for michi",20,5,5,10,10))


        //val usuarioLogueado = intent.extras?.getSerializable("USUARIO_LOGUEADO") as Profesore

        btnMateriasProfesor.setOnClickListener {
            val intent = Intent (this, MateriasProfesorActivity::class.java)
            //intent.putExtra("USUARIO_LOGUEADO", usuarioLogueado)
            startActivity(intent)
        }

        btnAlumnosTotalesProfesor.setOnClickListener {
            val intent = Intent (this, ResumenProfesorActivity::class.java)
            //intent.putExtra("USUARIO_LOGUEADO", usuarioLogueado)
            startActivity(intent)
        }


    }
}