package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import vs.layour.dadm_u1_proyectosge.adapters.MateriasAdapter
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.databinding.ActivityMateriasProfesorBinding
import vs.layour.dadm_u1_proyectosge.models.Profesore


class MateriasProfesorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMateriasProfesorBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMateriasProfesorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val daoprofesores = Utils.dataBaseSQL
        val profesor = LogInProfesorActivity.logProfesor
        //val bd = intent.extras?.getSerializable("BD") as dataBase
        //val usuarioLogueado = intent.extras?.getSerializable("USUARIO_LOGUEADO") as Profesore

        binding.textViewMisDatosNombre.text=profesor.nombre


        // val bd = AccederActivity.bd


        val materias = ArrayList<String>()
        for(i in  daoprofesores.consultaProfesor()){
            if(profesor.nombre == i.nombre){
                materias.add(i.materia)
            }

        }

        binding.lvMaterias.adapter = MateriasAdapter(this, R.layout.materia_item, materias)



    }
}