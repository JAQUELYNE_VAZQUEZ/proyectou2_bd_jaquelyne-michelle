package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.databinding.ActivityInicioBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase

class InicioActivity : AppCompatActivity() {


    private lateinit var binding: ActivityInicioBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInicioBinding.inflate(layoutInflater)
        setContentView(binding.root)
        var btnAcceder  = findViewById<Button>(R.id.btnInicio_acceder)

        /*val bd = intent.extras?.getSerializable("BD") as dataBase
        val daoAlumno = intent.extras?.getSerializable("ALUMNO") as Alumno
        for (i in AccederActivity.){
            println(i)
        }*/

        btnAcceder.setOnClickListener{
            val intent = Intent (this, MenuAlumnoActivity::class.java)
            startActivity(intent)
        }
    }
    
}