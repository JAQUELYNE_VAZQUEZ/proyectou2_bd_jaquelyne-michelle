package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import vs.layour.dadm_u1_proyectosge.adapters.AlumnosTotalesAdapter
import vs.layour.dadm_u1_proyectosge.adapters.ComentariosDeEvaluacionAdapter
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.data.DAOEvaluacion
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.databinding.ActivityResumenProfesorBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.Profesore
import vs.layour.dadm_u1_proyectosge.models.dataBase


class ResumenProfesorActivity : AppCompatActivity() {


    private lateinit var binding: ActivityResumenProfesorBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityResumenProfesorBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //val bd = AccederActivity.bd

        val alumnos = Utils.dataBaseSQL
        val evaluacionesDocentes = Utils.dataBaseSQL
        val logprofesor = LogInProfesorActivity.logProfesor

        //val usuarioLogueado = intent.extras?.getSerializable("USUARIO_LOGUEADO") as Profesore



        //For anidado, para comprara el nControl del evaluador con el alumno
        val alumnosEvaluadores = ArrayList<Alumno>()
        for (i in alumnos.consultaAlumnos()){
            for (evaluador in evaluacionesDocentes.getEvaluaciones()){
                if (i.noControl.equals(evaluador.noControlAlumno)){
                    alumnosEvaluadores.add(i)

                }
            }
        }
        val numAlumnosTotales = alumnos.consultaAlumnos().size
        val numalumnosEvaluadores = evaluacionesDocentes.consultaEvaluacion().size

        println("$numAlumnosTotales  $numalumnosEvaluadores")

        binding.numAlumnosTotales.text = numAlumnosTotales.toString()
        binding.numAlumnosEvaluadores.text = numalumnosEvaluadores.toString()

        //pinta todos los alumnos del profesor
        binding.lvAlumnosTotales.adapter = AlumnosTotalesAdapter(this, R.layout.total_alumnos_item, alumnos.consultaAlumnos())
        val evaluaciones = ArrayList<Evaluacion>()
        for(i in evaluacionesDocentes.getEvaluaciones()){
            if (i.profesor==logprofesor.nombre){
                evaluaciones.add(i)
            }
        }
        // pintar todos los alumnos que evaluaron al docente
        binding.lvAlumnosEvaluaron.adapter = AlumnosTotalesAdapter(this, R.layout.total_alumnos_item, alumnosEvaluadores)

        //Se pintan numero de los comentarios
        binding.lvComentarios.adapter = ComentariosDeEvaluacionAdapter(this, R.layout.comentarios_item,evaluaciones)
        //  COMENTARIO
        binding.numComentarios.setText(evaluaciones.size.toString())


        //-------------------------------GRAFICA -------------------------------



        val listpie = ArrayList<PieEntry>()
        val listColors= ArrayList<Int>()



        listpie.add(PieEntry(20f, "Catedra"))
        listColors.add(resources.getColor(R.color.aExamenParcial))

        listpie.add(PieEntry(20f, "evaluacion"))
        listColors.add(resources.getColor(R.color.azulMarino))

        listpie.add(PieEntry(20f, "accesibilidad"))
        listColors.add(resources.getColor(R.color.materiaAcreditada))

        listpie.add(PieEntry(20f, "calidad"))
        listColors.add(resources.getColor(R.color.materiaPosible))

        listpie.add(PieEntry(20f, "profesionalismo"))
        listColors.add(resources.getColor(R.color.cursadaSinAcreditar))



        val piedataset=PieDataSet(listpie, "")
        piedataset.colors = listColors


        binding.piechartView.data = PieData(piedataset)

/*

        //LA OTRA GRAFICA ANDROID XD
        val listpie = ArrayList<Int>()
        val s1 = Segment("Uno", listpie.get(0))
        val s2 = Segment("dos", listpie.get(1))
        val s3 = Segment("tres", listpie.get(2))
        val s4 = Segment("cuatro", listpie.get(3))

        val sf1 = SegmentFormatter(Color.BLUE)
        val sf2 = SegmentFormatter(Color.YELLOW)
        val sf3 = SegmentFormatter(Color.RED)
        val sf4 = SegmentFormatter(Color.GREEN)

        pieChart.addSegment(s1,sf1)


*/
    }
}