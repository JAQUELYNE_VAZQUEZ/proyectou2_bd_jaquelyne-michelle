package vs.layour.dadm_u1_proyectosge
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.gson.Gson
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.data.DAOEvaluacion
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.data.DataBaseSQL


import vs.layour.dadm_u1_proyectosge.models.dataBase

class  AccederActivity : AppCompatActivity() {
    lateinit var btnAccederAlumno: Button
    lateinit var btnAccederProfesor: Button

    /* companion object {
         lateinit var bd: dataBase
     }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acceder)

        Utils.dataBaseSQL = DataBaseSQL(this, "BD", null, 1)

        //-----------------OOBTENCION DE LA BD ATRAVES DE QUERY---------------------
        //Utils.daoProfesor = DAOProfesor(this, "BD", null, 1)
        //Utils.daoAlumno = DAOAlumno(this, "BD", null, 1)
        //Utils.daoEvaluacion= DAOEvaluacion(this,"BD",null,1)

        //------------------------BASE DE DATOS--------------------------------------
        //Obtención de la base de datos completa
        //val bdString = resources.getString(R.string.baseDeDatos)
        //bd = Gson().fromJson(bdString, dataBase::class.java) as dataBase

        btnAccederAlumno = findViewById<Button>(R.id.btnInicioAlumno)
        btnAccederProfesor = findViewById<Button>(R.id.btnInicioProfesor)

        //  val profesor = Utils.daoProfesor.getProfesor()
        //  val alumno = Utils.daoAlumno.getAlumno()

        btnAccederAlumno.setOnClickListener {
            val intent = Intent (this, LogInAlumnoActivity::class.java)
            // intent.putExtra("BD", bd)
            startActivity(intent)
        }

        btnAccederProfesor.setOnClickListener {
            val intent = Intent (this, LogInProfesorActivity::class.java)
            //intent.putExtra("BD", bd)
            startActivity(intent)
        }
    }


}