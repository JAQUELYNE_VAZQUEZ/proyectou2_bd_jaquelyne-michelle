package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioButton
import android.widget.Toast
import com.google.gson.Gson
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.data.DAOEvaluacion
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.data.DataBaseSQL
import vs.layour.dadm_u1_proyectosge.databinding.ActivityEvaluacionDocenteBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.PreguntaJson
import vs.layour.dadm_u1_proyectosge.models.dataBase
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class EvaluacionDocenteActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEvaluacionDocenteBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEvaluacionDocenteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Utils.dataBaseSQL = DataBaseSQL(this, "BD", null, 1)
        val dataBase = Utils.dataBaseSQL
        val profesores = Utils.dataBaseSQL.consultaProfesor()
        val evaluacionesDocentes = Utils.dataBaseSQL.consultaEvaluacion()


        //val bd = intent.extras?.getSerializable("BD") as dataBase
        //val alumno = intent.extras?.getSerializable("DAOALUMNO") as Alumno

        //Se recorre el json de preguntas y se asignan
        val bdString = resources.getString(R.string.JsonPreguntas)
        val bdJsonPreguntas = Gson().fromJson(bdString, PreguntaJson::class.java)
        val preguntas = bdJsonPreguntas
        //println(preguntas)

        val arregloProfesores = profesores
        val nombreProfesores = ArrayList<String>()
        var profesorSeleccionado = ""

        //Se recorre la bdCompleta, para asignar los profesores en el spinner
        nombreProfesores.add("-seleccione un profesor-")
        for (i in arregloProfesores) {
            val nombre = i.nombre
            nombreProfesores.add(nombre)
        }

        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item, nombreProfesores
        )

        binding.spProfesor.adapter = adapter

        binding.spProfesor.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View, position: Int, id: Long
            ) {
                profesorSeleccionado = nombreProfesores[position]

                Toast.makeText(
                    this@EvaluacionDocenteActivity,
                    "El seleccionado es $profesorSeleccionado",
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }


        val pregunta1 = preguntas.preguntas[0]
        binding.Preguna1.text = pregunta1.pregunta

        val pregunta2 = preguntas.preguntas[1]
        binding.Preguna2.text = pregunta2.pregunta

        val pregunta3 = preguntas.preguntas[2]
        binding.Preguna3.text = pregunta3.pregunta

        val pregunta4 = preguntas.preguntas[3]
        binding.Preguna4.text = pregunta4.pregunta

        val pregunta5 = preguntas.preguntas[4]
        binding.Preguna5.text = pregunta5.pregunta

        val pregunta6 = preguntas.preguntas[5]
        binding.Preguna6.text = pregunta6.pregunta

        val pregunta7 = preguntas.preguntas[6]
        binding.Preguna7.text = pregunta7.pregunta

        val pregunta8 = preguntas.preguntas[7]
        binding.Preguna8.text = pregunta8.pregunta

        val pregunta9 = preguntas.preguntas[8]
        binding.Preguna9.text = pregunta9.pregunta

        val pregunta10 = preguntas.preguntas[9]
        binding.Preguna10.text = pregunta10.pregunta



        var calificacion: Int = 0
        var respuestasCompletas = true

        binding.btnEvaluacionDocenteEnviar.setOnClickListener {
            /*Radio Group 1*/
            //Se obtiene el id del radioButton seleccionado en el radioGroup
            var rg1: Int = binding.rg1.checkedRadioButtonId
            if (rg1 != -1) {
                // Se obtiene la instancia del radioButton con el id
                val radio: RadioButton = findViewById(rg1)

                calificacion += radio.text.toString().toInt()

            } else {
                respuestasCompletas = false
            }
            /*Radio Group 2*/

            //Verificación del radioButton que se sselacciona para la obtención de calificación
            var calificacion: Int = 0
            var respuestasCompletas = true

            // Se agrgan para obtener la calificacion de cada rubro
            var catedraRubro = 0
            var evaluacionRubro = 0
            var accecibilidadRubro = 0
            var calidadRubro = 0
            var profesionalismoRubro = 0

            binding.btnEvaluacionDocenteEnviar.setOnClickListener {

                //Se obtiene el id del radioButton seleccionado en el radioGroup
                var rg1: Int = binding.rg1.checkedRadioButtonId
                if (rg1 != -1) {
                    // Se obtiene la instancia del radioButton con el id
                    val radio: RadioButton = findViewById(rg1)

                    calidadRubro += radio.text.toString().toInt()

                } else {
                    respuestasCompletas = false
                }

                var rg2: Int = binding.rg2.checkedRadioButtonId
                if (rg2 != -1) {
                    val radio: RadioButton = findViewById(rg2)
                    profesionalismoRubro += radio.text.toString().toInt()
                } else {

                    respuestasCompletas = false
                }

                /*Radio Group 3*/

                var rg3: Int = binding.rg3.checkedRadioButtonId
                if (rg3 != -1) {
                    val radio: RadioButton = findViewById(rg3)
                    calidadRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 4*/

                var rg4: Int = binding.rg4.checkedRadioButtonId
                if (rg4 != -1) {
                    val radio: RadioButton = findViewById(rg4)
                    catedraRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 5*/

                var rg5: Int = binding.rg5.checkedRadioButtonId
                if (rg5 != -1) {
                    val radio: RadioButton = findViewById(rg5)
                    evaluacionRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 6*/

                var rg6: Int = binding.rg6.checkedRadioButtonId
                if (rg6 != -1) {
                    val radio: RadioButton = findViewById(rg6)
                    catedraRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 7*/

                var rg7: Int = binding.rg7.checkedRadioButtonId
                if (rg7 != -1) {
                    val radio: RadioButton = findViewById(rg7)
                    catedraRubro+= radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 8*/


                var rg8: Int = binding.rg8.checkedRadioButtonId
                if (rg8 != -1) {
                    val radio: RadioButton = findViewById(rg8)
                    catedraRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 9*/


                var rg9: Int = binding.rg9.checkedRadioButtonId
                if (rg9 != -1) {
                    val radio: RadioButton = findViewById(rg9)
                    accecibilidadRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }

                /*Radio Group 10*/

                var rg10: Int = binding.rg10.checkedRadioButtonId
                if (rg10 != -1) {
                    val radio: RadioButton = findViewById(rg10)
                    profesionalismoRubro += radio.text.toString().toInt()
                } else {
                    respuestasCompletas = false
                }


                if (rg1 == -1 ||
                    rg2 == -1 ||
                    rg3 == -1 ||
                    rg4 == -1 ||
                    rg5 == -1 ||
                    rg6 == -1 ||
                    rg7 == -1 ||
                    rg8 == -1 ||
                    rg9 == -1 ||
                    rg10 == -1
                ) {
                    Toast.makeText(this, "Verifica tus respuestas", Toast.LENGTH_SHORT).show()
                } else {
                    val newId = evaluacionesDocentes.size + 1
                    val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
                    val fechaActual = sdf.format(Date())
                    val comentario = binding.editTextComentario.text.toString()


                    //llama variable global db
                        dataBase.altaEvaluacion(
                        Evaluacion(
                            newId,
                            profesorSeleccionado,
                            LogInAlumnoActivity.logalumno.noControl,
                            fechaActual,
                            comentario,
                            catedraRubro,
                            evaluacionRubro,
                            accecibilidadRubro,
                            calidadRubro,
                            profesionalismoRubro
                        )
                    )

                    Toast.makeText(this, "Se ha enviado la evaluacion", Toast.LENGTH_SHORT).show()
                    this.finish()
                }
            }

        }
    }
}
