package vs.layour.dadm_u1_proyectosge.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.Profesore

class DAOEvaluacion (
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version) {
    override fun onCreate(db: SQLiteDatabase?) {
        val evaluacion = """
            create table evaluacion(
            id integer not null primary key autoincrement,
            profesor text not null,
            noControlAlumno text not null,
            horaCreacion text not null,
            comentarios text not null,
            r1 integer not null,
            r2 integer not null,
            r3 integer not null,
            r4 integer not null,
            r5 integer not null
        );
        """.trimMargin()

        db?.let {
            it.execSQL(evaluacion)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    @Throws
    fun getEvaluaciones( ): ArrayList<Evaluacion> {
        val db = readableDatabase

        val sql = "SELECT * FROM evaluacion "

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Evaluacion>()
        while (cursor.moveToNext()) {
            val evaluacion = Evaluacion(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8),
                cursor.getInt(9),
            )
            resultados.add(evaluacion)
        }
        db.close()

        return resultados
    }

    @Throws
    fun altaEvaluacion(evaluacion: Evaluacion) {
        val db = writableDatabase

        val sql =
            "INSERT INTO evaluacion (id, profesor, noControlAlumno, horaCreacion, comentarios, r1, r2, r3, r4, r5)" +
                    " VALUES ('${evaluacion.id}', '${evaluacion.profesor}', '${evaluacion.noControlAlumno}','${evaluacion.horaCreacion}', '${evaluacion.comentarios}', '${evaluacion.catedraRubro}', '${evaluacion.evaluacionRubro}', '${evaluacion.accesibilidadRubro}', '${evaluacion.calidadRubro}', '${evaluacion.profesionalismoRubro}')"
        db.execSQL(sql)

        db.close()
    }


    @Throws
    fun consultaEvaluacion(): ArrayList<Evaluacion> {
        val db = readableDatabase

        val sql = "SELECT id, profesor, noControlAlumno, horaCreacion, comentarios, r1, r2, r3, r4, r5  FROM evaluacion"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Evaluacion>()
        while (cursor.moveToNext()) {
            val evaluacion = Evaluacion(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8),
                cursor.getInt(9)

            )

            resultados.add(evaluacion)
        }
        db.close()

        return resultados
    }
}