package vs.layour.dadm_u1_proyectosge.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Profesore

class DAOProfesor (
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version) {
    override fun onCreate(db: SQLiteDatabase?) {
        val profesores = """
            create table profesor(
            nombre text not null,
            id text not null,
            contrasena text not null,
            materia text not null 
        );
        """.trimMargin()

        db?.let {
            it.execSQL(profesores)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    @Throws
    fun getProfesor(profesore: Profesore): List<Profesore> {
        val db = readableDatabase

        val sql = "SELECT * FROM profesor WHERE '${profesore.id}'"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Profesore>()
        while (cursor.moveToNext()) {
          val profesor = Profesore(
              cursor.getString(0),
              cursor.getString(1),
              cursor.getString(2),
              cursor.getString(3),
          )
            resultados.add(profesor)
        }
        db.close()

        return resultados
    }


    @Throws
    fun altaProfesor(profesore: Profesore) {
        val db = writableDatabase

        val sql =
            "INSERT INTO profesor (nombre, id, contrasena, materia)" +
                    " VALUES ('${profesore.nombre}', '${profesore.id}', '${profesore.contrasena}','${profesore.materia}')"
        db.execSQL(sql)

        db.close()
    }


    @Throws
    fun consultaProfesor(): ArrayList<Profesore> {
        val db = readableDatabase

        val sql = "SELECT nombre, id, contrasena, materia FROM profesor"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Profesore>()
        while (cursor.moveToNext()) {
            val profesor = Profesore(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3)

            )

            resultados.add(profesor)
        }
        db.close()

        return resultados
    }
}