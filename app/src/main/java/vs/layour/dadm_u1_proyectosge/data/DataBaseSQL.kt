package vs.layour.dadm_u1_proyectosge.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Evaluacion
import vs.layour.dadm_u1_proyectosge.models.Profesore


class DataBaseSQL (context: Context?,
                   name: String?,
                   factory: SQLiteDatabase.CursorFactory?,
                   version: Int
) : SQLiteOpenHelper(context, name, factory, version) {
    override fun onCreate(db: SQLiteDatabase?) {
        //QRY DE TABLA ALUMNOS
        val alumnos = """
            create table alumno(
            nombre text not null,
            noControl text not null,
            carrera text not null,
            semestre text not null,
            curp text not null,
            fNacimiento text not null, 
            sexo text not null, 
            correoPersonal text not null,
            correoInstitucional text not null, 
            grupo text not null, 
            contrasena text not null
        );
        """.trimMargin()
        //QRY DE TABLA EVALUACIONES
        val evaluacion = """
            create table evaluacion(
            id integer not null primary key autoincrement,
            profesor text not null,
            noControlAlumno text not null,
            horaCreacion text not null,
            comentarios text not null,
            r1 integer not null,
            r2 integer not null,
            r3 integer not null,
            r4 integer not null,
            r5 integer not null
        );
        """.trimMargin()
        //QRY DE TABLA PROFESORES
        val profesores = """
            create table profesor(
            nombre text not null,
            id text not null,
            contrasena text not null,
            materia text not null 
        );
        """.trimMargin()
        //SE DAN DE ALTA LA TABLAS DE LA BASE DE DATOS
        db?.let {
            it.execSQL(alumnos)
            it.execSQL(evaluacion)
            it.execSQL(profesores)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    //--------------------------ALUMNO ----------------------------------------//
    @Throws
    fun getAlumno(alumno: Alumno): ArrayList<Alumno> {
        val db = readableDatabase

        val sql = "SELECT * FROM alumno WHERE '${alumno.noControl}'"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Alumno>()
        while (cursor.moveToNext()) {
            val alumno = Alumno(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9),
                cursor.getString(10),
            )
            resultados.add(alumno)
        }
        db.close()

        return resultados
    }

    @Throws
    fun altaAlumno(alumno: Alumno) {
        val db = writableDatabase

        val sql =
            "INSERT INTO alumno (noControl,nombre,semestre,carrera, curp, fNacimiento, sexo,  correoPersonal, correoInstitucional, grupo, contrasena)" +
                    " VALUES ('${alumno.noControl}', '${alumno.nombre}', '${alumno.semestre}','${alumno.carrera}', '${alumno.curp}','${alumno.fNacimiento}', '${alumno.sexo}', '${alumno.correoPersonal}','${alumno.correoInstitucional}','${alumno.grupo}', '${alumno.contrasena}')"
        db.execSQL(sql)

        db.close()
    }

    @Throws
    fun consultaAlumnos(): ArrayList<Alumno> {
        val db = readableDatabase

        val sql = "SELECT nombre,noControl,carrera,semestre,curp,fNacimiento, sexo, correoPersonal, correoInstitucional, grupo, contrasena FROM alumno"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Alumno>()
        while (cursor.moveToNext()) {
            val alumno = Alumno(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9),
                cursor.getString(10)


            )

            resultados.add(alumno)
        }
        db.close()

        return resultados
    }

    //-------------------------- PROFESOR ----------------------------------------//
    @Throws
    fun getProfesor(profesore: Profesore): List<Profesore> {
        val db = readableDatabase

        val sql = "SELECT * FROM profesor WHERE '${profesore.id}'"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Profesore>()
        while (cursor.moveToNext()) {
            val profesor = Profesore(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
            )
            resultados.add(profesor)
        }
        db.close()

        return resultados
    }


    @Throws
    fun altaProfesor(profesore: Profesore) {
        val db = writableDatabase

        val sql =
            "INSERT INTO profesor (nombre, id, contrasena, materia)" +
                    " VALUES ('${profesore.nombre}', '${profesore.id}', '${profesore.contrasena}','${profesore.materia}')"
        db.execSQL(sql)

        db.close()
    }


    @Throws
    fun consultaProfesor(): ArrayList<Profesore> {
        val db = readableDatabase

        val sql = "SELECT nombre, id, contrasena, materia FROM profesor"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Profesore>()
        while (cursor.moveToNext()) {
            val profesor = Profesore(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3)

            )

            resultados.add(profesor)
        }
        db.close()

        return resultados
    }

    //-------------------------- EVALUACION ----------------------------------------//

    @Throws
    fun getEvaluaciones( ): ArrayList<Evaluacion> {
        val db = readableDatabase

        val sql = "SELECT * FROM evaluacion "

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Evaluacion>()
        while (cursor.moveToNext()) {
            val evaluacion = Evaluacion(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8),
                cursor.getInt(9),
            )
            resultados.add(evaluacion)
        }
        db.close()

        return resultados
    }

    @Throws
    fun altaEvaluacion(evaluacion: Evaluacion) {
        val db = writableDatabase

        val sql =
            "INSERT INTO evaluacion (id, profesor, noControlAlumno, horaCreacion, comentarios, r1, r2, r3, r4, r5)" +
                    " VALUES ('${evaluacion.id}', '${evaluacion.profesor}', '${evaluacion.noControlAlumno}','${evaluacion.horaCreacion}', '${evaluacion.comentarios}', '${evaluacion.catedraRubro}', '${evaluacion.evaluacionRubro}', '${evaluacion.accesibilidadRubro}', '${evaluacion.calidadRubro}', '${evaluacion.profesionalismoRubro}')"
        db.execSQL(sql)

        db.close()
    }


    @Throws
    fun consultaEvaluacion(): ArrayList<Evaluacion> {
        val db = readableDatabase

        val sql = "SELECT id, profesor, noControlAlumno, horaCreacion, comentarios, r1, r2, r3, r4, r5  FROM evaluacion"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Evaluacion>()
        while (cursor.moveToNext()) {
            val evaluacion = Evaluacion(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5),
                cursor.getInt(6),
                cursor.getInt(7),
                cursor.getInt(8),
                cursor.getInt(9)

            )

            resultados.add(evaluacion)
        }
        db.close()

        return resultados
    }
}