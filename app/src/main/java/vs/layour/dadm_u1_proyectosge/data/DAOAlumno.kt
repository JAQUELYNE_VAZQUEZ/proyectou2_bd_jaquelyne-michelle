package vs.layour.dadm_u1_proyectosge.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Profesore

class DAOAlumno(
    context: Context?,
    name: String?,
    factory: SQLiteDatabase.CursorFactory?,
    version: Int
) : SQLiteOpenHelper(context, name, factory, version) {

    override fun onCreate(db: SQLiteDatabase?) {
        val alumnos = """
            create table alumno(
            nombre text not null,
            noControl text not null,
            carrera text not null,
            semestre text not null,
            curp text not null,
            fNacimiento text not null, 
            sexo text not null, 
            correoPersonal text not null,
            correoInstitucional text not null, 
            grupo text not null, 
            contrasena text not null
        );
        """.trimMargin()

        db?.let {
            it.execSQL(alumnos)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

    @Throws
    fun getAlumno(alumno: Alumno): ArrayList<Alumno> {
        val db = readableDatabase

        val sql = "SELECT * FROM alumno WHERE '${alumno.noControl}'"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Alumno>()
        while (cursor.moveToNext()) {
            val alumno = Alumno(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9),
                cursor.getString(10),
            )
            resultados.add(alumno)
        }
        db.close()

        return resultados
    }

    @Throws
    fun altaAlumno(alumno: Alumno) {
        val db = writableDatabase

        val sql =
            "INSERT INTO alumno (noControl,nombre,semestre,carrera, curp, fNacimiento, sexo,  correoPersonal, correoInstitucional, grupo, contrasena)" +
                    " VALUES ('${alumno.noControl}', '${alumno.nombre}', '${alumno.semestre}','${alumno.carrera}', '${alumno.curp}','${alumno.fNacimiento}', '${alumno.sexo}', '${alumno.correoPersonal}','${alumno.correoInstitucional}','${alumno.grupo}', '${alumno.contrasena}')"
        db.execSQL(sql)

        db.close()
    }

    @Throws
    fun consultaAlumnos(): ArrayList<Alumno> {
        val db = readableDatabase

        val sql = "SELECT nombre,noControl,carrera,semestre,curp,fNacimiento, sexo, correoPersonal, correoInstitucional, grupo, contrasena FROM alumno"

        val cursor = db.rawQuery(sql, null)

        val resultados = ArrayList<Alumno>()
        while (cursor.moveToNext()) {
            val alumno = Alumno(
                cursor.getString(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6),
                cursor.getString(7),
                cursor.getString(8),
                cursor.getString(9),
                cursor.getString(10)


            )

            resultados.add(alumno)
        }
        db.close()

        return resultados
    }

}