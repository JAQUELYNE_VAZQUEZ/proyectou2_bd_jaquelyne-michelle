package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.gson.Gson
import org.json.JSONObject
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.data.DAOProfesor
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.Profesore

class LogInProfesorActivity : AppCompatActivity() {
    lateinit var editId: EditText
    lateinit var editPass: EditText
    lateinit var btnAcceder: Button
    companion object {
        lateinit var logProfesor: Profesore
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in_profesor)




        editId = findViewById(R.id.editTextLogInProfesor_Id)
        editPass = findViewById(R.id.editTextLogInProfesor_password)
        btnAcceder = findViewById(R.id.btnLogInProfesor_iniciarsesion)

        //val miJson = resources.getString(R.string.baseDeDatos)
        //val jsonProfesores = JSONObject(miJson)
        //val arrayProfesores = jsonProfesores.getJSONArray("profesores")

        val database = Utils.dataBaseSQL
        //Alta de Profesores
       // database.altaProfesor(Profesore(" lua vargas", "120", "123", "sistemas operativos 2"))
       // database.altaProfesor(Profesore(" Alberto Pintor", "123456", "123", "Ciclos de Vida de Software"))
       // database.altaProfesor(Profesore("Octavio Salud ", "333", "123", "Taller de Investigacion 1"))
       // database.altaProfesor(Profesore("Jorge Mora ", "3334", "123", "Redes Emergentes"))

        btnAcceder.setOnClickListener {
            val id = editId.text.toString()
            val pass = editPass.text.toString()

            var acceso = false
            for (prof in database.consultaProfesor()) {
                //print(arrayProfesores[i].toString())

                //val jsonProfesor = arrayProfesores.getJSONObject(i)

                if (prof.id.trim().equals(id.trim())) {
                    acceso = true

                    if (prof.contrasena.trim().equals(pass.trim())) {
                        Toast.makeText(this, "Encontrado", Toast.LENGTH_LONG).show()

                        val intent = Intent(this, MenuProfesorActivity::class.java)
                        //intent.putExtra("profesor", jsonProfesor.toString())
                        //intent.putExtra("bd", miJson)
                        //val prof = Gson().fromJson(i.toString(), Profesore::class.java)

                        //println("----------------")
                        //println("$prof")
                        logProfesor=prof
                        intent.putExtra("PROFESOR", prof)
                        //intent.putExtra("USUARIO_LOGUEADO", prof)
                        startActivity(intent)
                        //finish()

                    } else {
                        Toast.makeText(this, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                    }
                }
            }
            if (!acceso) {
                Toast.makeText(this, "No Existen estos datos", Toast.LENGTH_LONG).show()

            }
        }
    }

}

