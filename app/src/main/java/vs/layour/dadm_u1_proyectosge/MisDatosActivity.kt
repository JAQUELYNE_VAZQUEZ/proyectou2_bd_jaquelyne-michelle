package vs.layour.dadm_u1_proyectosge

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import vs.layour.dadm_u1_proyectosge.databinding.ActivityMisDatosBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno

class MisDatosActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMisDatosBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMisDatosBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //  var bdString = resources.getString(R.string.baseDeDatos)
        // val mibd = Gson().fromJson(bdString, dataBase::class.java)
        //  val alumno = mibd.alumnos[0]

        val alumno = Utils.dataBaseSQL
        val logalumno = LogInAlumnoActivity.logalumno



        //val alumno = intent.extras?.getSerializable("ALUMNO")as Alumno
        //println(alumno)

        binding.textViewMisDatosNombre.text= logalumno.nombre
        binding.txtViewCurp.text = logalumno.curp
        binding.txtfNacimiento.text = logalumno.fNacimiento
        binding.txtsexo.text = logalumno.sexo
        binding.txtViewMisDatosNControl.text =logalumno.noControl
        binding.txtViewSemestre.text= logalumno.semestre
        binding.txtViewCarrera.text = logalumno.carrera
        binding.txtcorreopersonal.text = logalumno.correoPersonal
        binding.txtcorreoinstitucional.text = logalumno.correoInstitucional







    }
}