package vs.layour.dadm_u1_proyectosge

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import org.json.JSONObject
import vs.layour.dadm_u1_proyectosge.data.DAOAlumno
import vs.layour.dadm_u1_proyectosge.databinding.ActivityLogInAlumnoBinding
import vs.layour.dadm_u1_proyectosge.models.Alumno
import vs.layour.dadm_u1_proyectosge.models.dataBase


class LogInAlumnoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLogInAlumnoBinding

    lateinit var editNoControl: EditText
    lateinit var editpass: EditText
    lateinit var btnAcceder: Button

    companion object {
        lateinit var logalumno: Alumno
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val database = Utils.dataBaseSQL

        binding = ActivityLogInAlumnoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Alta de alumnos
        //database.altaAlumno(Alumno("Michelle", "18120225", "itics", "4", "NOMELAC1997", "211197", "femenino", "correo@gmail.com", "ponymail@gmail.com", "b", "123"))
        //database.altaAlumno(Alumno("Jaquelyne", "18120237", "itics", "4", "NOMELAC1997", "301299", "femenino", "correo@gmail.com", "ponymail@gmail.com", "a", "123"))
        //database.altaAlumno(Alumno("Joe", "18120215", "itics", "4", "NOMELAC1997", "301299", "femenino", "correo@gmail.com", "ponymail@gmail.com", "a", "123"))
       // database.altaAlumno(Alumno("Juanita", "18120216", "itics", "4", "NOMELAC1997", "301299", "femenino", "correo@gmail.com", "ponymail@gmail.com", "a", "123"))

       // val bd = intent.extras?.getSerializable("BD") as dataBase

        editNoControl = findViewById(R.id.editTextLogInAlumno_NControl)
        editpass = findViewById(R.id.editTextLogInAlumno_password)
        btnAcceder = findViewById(R.id.btnLogInAlumno_iniciarsesion)

        // val miJson = resources.getString(R.string.baseDeDatos)
        // val jsonAlumnos = JSONObject(miJson)
        // val arrayAlumnos = jsonAlumnos.getJSONArray("alumnos")

        btnAcceder.setOnClickListener {
            val numcontrol = editNoControl.text.toString()
            val pass = editpass.text.toString()

            var acceso = false
            for (i in database.consultaAlumnos()) {


                //  val jsonEstudiante = arrayAlumnos.getJSONObject(i)
                if (i.noControl.equals(numcontrol.trim())) {
                    acceso = true

                    if (i.contrasena.trim().equals(pass.trim())) {
                        Toast.makeText(this, "Encontrado", Toast.LENGTH_LONG).show()

                        logalumno = i
                        // se manda el objeto alumno a la sig activity
                        //println(arrayAlumnos[i].toString())
                        //val alumno = Gson().fromJson(arrayAlumnos[i].toString(), Alumno::class.java)

                        val intent = Intent (this, InicioActivity::class.java)

                        intent.putExtra("ALUMNO", i)
                        //intent.putExtra("BD", bd)

                        startActivity(intent)
                        //finish()

                    } else {
                        Toast.makeText(this, "Datos Incorrectos", Toast.LENGTH_LONG).show()
                    }
                }
            }
            if (!acceso){
                Toast.makeText(this,"No Existen estos datos",Toast.LENGTH_LONG).show()

            }
        }

    }
}